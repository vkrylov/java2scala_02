package bcrypt

import com.github.t3hnar.bcrypt.BCryptStrOps
import com.typesafe.scalalogging.StrictLogging
import util.ExecutionLogging

import scala.concurrent.{ExecutionContext, Future}

trait AsyncBcrypt {

  def hash(password: String, rounds: Int = 12): Future[String]

  def verify(password: String, hash: String): Future[Boolean]

}

class AsyncBcryptImpl()(implicit ec: ExecutionContext) extends AsyncBcrypt with StrictLogging with ExecutionLogging {

  override def hash(password: String, rounds: Int): Future[String] =
    Future {
      withExecutionLogging(s"hashing $password")(password.bcrypt(rounds))
    }

  override def verify(password: String, hash: String): Future[Boolean] =
    Future {
      withExecutionLogging(s"verifying $password")(password.isBcrypted(hash))
    }

}